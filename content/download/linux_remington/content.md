+++
fragment = "content"
date = "2021-09-14"
weight = 130

title = "GNU/Linux Remington"

[sidebar]
  enable = true
  sticky = true
+++

To use Extended Remington Layout for linux you need IBus in your sistem
## Install IBus
install these packages\
`ibus ibus-m17n m17n-db`
if you are using ubuntu run this in terminal\
`sudo apt install ibus ibus-m17n m17n-db`\
for more detailed info refer [swanalekha documentation](https://swanalekha.smc.org.in/desktop/linux/#linux)
## Setting Up IBus

Most GNOME systems are well-integrated with IBus by default.

If you're not using GNOME, you will need to setup IBus. ArchLinux Wiki has good information on setting up IBus: https://wiki.archlinux.org/title/IBus

IBus setup might be a bit tricky in KDE. It can be easily setup in ArchLinux by installing `ibus-input-support`.

## Install poorna
clone the [repositoty](https://gitlab.com/smc/poorna/poorna-remington-linux)\
copy ml-poornaremington.mim to /usr/share/m17n\

it can be done through following commands in terminal
```
git clone https://gitlab.com/smc/poorna/poorna-remington-linux.git
cd poorna-remington-liux
sudo cp ml-poornaremington.mim /usr/share/m17n
```
then add the layout to your Ibus preferences.\
in ubuntu you can add keyboard from settings->keyboard

## Remember this
to work 4 layers in poorna you have to select Right Alt from alternate charecter key option
![ubuntu altgr](altgr-ubuntu.png)
