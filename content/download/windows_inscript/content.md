+++
fragment = "content"
date = "2021-09-01"
weight = 150

title = "Windows Inscript"

[sidebar]
  enable = true
  sticky = true
+++

# Install
Download the [Poorna Inscript  Windows release](https://gitlab.com/smc/poorna/poorna-releases/-/raw/main/poorna_inscript_windows.zip) file\
unzip\
Double click setup to install\
then you can use it from windows taskbar.

if your windows system not allowing to install click more info and allow for run.