+++
fragment = "content"
date = "2021-12-26"
weight = 140

title = "macOS Remington"

[sidebar]
  enable = true
  sticky = true
+++

## Install
Download [Poorna Remington Mac](https://gitlab.com/smc/poorna/poorna-releases/-/raw/main/poorna_remington_mac.zip) file\
unzip\
press `Cmd+Shift+G`, and type `“/Library/Keyboard Layouts/”` to navigate to the Keyboard Layouts,
paste the poorna bundle here then add keyboard layout from keyboard settings.