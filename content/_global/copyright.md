+++
fragment = "copyright"
#disabled = true
date = "2016-09-07"
weight = 1250
background = "grey"

copyright =  "A project from Swathanthra Malayalam Computing | GPL Licensed | Copyright © 2022-present Mujeeb Cpy"
attribution = true # enable attribution by setting it to true
+++
