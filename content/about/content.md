+++
fragment = "content"
#disabled = true
date = "2017-10-05"
weight = 100
#background = ""

title = "About Poorna"
#subtitle = ""
+++

Poorna is an extended Inscript/Remington keyboard layout which available in Windows, Linux and Mac. it contains all malayalam unicode Characters. Read more in malayalam [smc blog](https://blog.smc.org.in/poorna-malayalam-keyboard-release/)

## Credits

{{< table "table table-striped" >}}
| Role  | Name |
|---------|--------|
| Idea     | Baiju Muthukadan   |
| Project Coordination     | Jaisen Nedumpala   |
| Developer | Mujeeb CPY  |
|Academic Help | Mahesh Mangalat|
| Logo | Hiran Venugopalan |
| Technical Support | SMC |
| Sponsor for version 1.0 |  [<img src="myg.jpg" width="70"/>](https://www.myg.in/)
{{</ table >}}