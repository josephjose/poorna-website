+++
fragment = "hero"
#disabled = true
date = "2022-12-28"
weight = 200
background = "light" # can influence the text color
particles = false

title = ""
subtitle = "Extended Inscript Layout"

[asset]
  image = "poorna_inscript.png"
+++