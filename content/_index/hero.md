+++
fragment = "hero"
#disabled = true
date = "2022-12-27"
weight = 50
background = "dark" # can influence the text color
particles = true

title = "Poorna Keyboard"
subtitle = "Malayalam Extended Inscript/Remington Keyboard"

[header]
  image = "header.jpg"

[asset]
  image = "logo.svg"
  width = "500px" # optional - will default to image width
  #height = "150px" # optional - will default to image height


[[buttons]]
  text = "Download"
  url = "/download"
  color = "primary"

[[buttons]]
  text = "Documentation"
  url = "/docs"
  color = "primary"
+++
