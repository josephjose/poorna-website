+++
fragment = "hero"
#disabled = true
date = "2022-12-28"
weight = 250
background = "light" # can influence the text color
particles = false

title = ""
subtitle = "Extended Remington Layout"

[asset]
  image = "poorna_remington.png"
+++